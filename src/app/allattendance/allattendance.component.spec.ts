import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllattendanceComponent } from './allattendance.component';

describe('AllattendanceComponent', () => {
  let component: AllattendanceComponent;
  let fixture: ComponentFixture<AllattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
