import { Component, OnInit } from '@angular/core';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-allattendance',
  templateUrl: './allattendance.component.html',
  styleUrls: ['./allattendance.component.css']
})
export class AllattendanceComponent implements OnInit {

  attendanceList = [];
  test = [0,1,2,3,4,5,6];
  name: string = "yolo";
  headers = [];
  
  constructor(private byService: ByServiceService) { }

  ngOnInit() {
    this.getAttendance();
  }

  getAttendance() {
    this.byService.getAttendance()
      .subscribe(
      (result) => {
        for (let i of result[0].subjects) {
          this.headers.push(i.name);
        }
     //   console.log(result[0])
        this.populateTable(result);
      },
      err => {
        console.log("error");
      }
      )
  }

  populateTable(r) {
    let temp = [];
    let arr = [];
    for (let i of r) {
      arr = [];
      let sum = "";
      let s = 0;
      let p = 0;
      let percent = "";
      for (let j of i.subjects) {
      
        p = j.hours_attended / j.hours_counted * 100;
        s = s + p;
        percent = p.toFixed(2);
      arr.push({"header": j.name, "field": percent})
      }
      s = s/arr.length
      sum = s.toFixed(2)
      temp.push({ "name": i.studentName, "subs": arr, "total": sum })


    }
  //  console.log(temp)
    this.attendanceList = temp;

  }

  calculateGroupTotal(car){
let total = 0;
for(let i of this.attendanceList){
  if(i.name == car){
    total = i.total;
  }
}
return total;
  }

}
