import { TestBed, inject } from '@angular/core/testing';

import { ByServiceService } from './by-service.service';

describe('ByServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ByServiceService]
    });
  });

  it('should be created', inject([ByServiceService], (service: ByServiceService) => {
    expect(service).toBeTruthy();
  }));
});
