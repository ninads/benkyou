import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkstemplateComponent } from './markstemplate.component';

describe('MarkstemplateComponent', () => {
  let component: MarkstemplateComponent;
  let fixture: ComponentFixture<MarkstemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkstemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkstemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
