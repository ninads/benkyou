import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ByServiceService } from '../by-service.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-markstemplate',
  templateUrl: './markstemplate.component.html',
  styleUrls: ['./markstemplate.component.css']
})
export class MarkstemplateComponent implements OnInit {

  name: string;
  searchForm: FormGroup;
  ddl: FormControl;
  inp: FormControl
  totcheck: number;
  subjectName: string;
  subjectFullName: string;
  subjectId: string;
  marksTemp: any;
  msgs = [];
  flag: boolean;
  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.subjectName = params['id'];
      this.marksTemp = [];
      this.getSubjects();
      this.searchForm = this.fb.group({
        properties: this.fb.array([])
      });
    });
  }


  initPropGroup() {
    return this.fb.group({
      name: [''],
      category: [''],
      total: ['']
    })
  }

  onAddProperty() {
    const control = <FormArray>this.searchForm.controls['properties'];
    control.push(this.initPropGroup())
  }

  getSubjects() {
    //  this.loading = true;
    this.byService.getSubjectByShortName(this.subjectName).subscribe(
      (result) => {
        let subjectsList = result;
        this.subjectId = subjectsList[0]._id.$oid;
        this.subjectFullName = subjectsList[0].subjectName.fullName;
        this.totcheck = subjectsList[0].totalInternalCredits;
        if (subjectsList[0].marksTemplate) {
          this.flag = true;
          this.marksTemp = subjectsList[0].marksTemplate;
        }
        else {
          this.flag = false;
        }
      },
      (err) => {
        console.log("error");
      }
    )
  }

  postTemplate() {
    let formdata = this.searchForm.value.properties
    console.log(formdata)
    let tot = 0;
    for (let i of formdata) {
      tot += Number(i.total);
    }
    if (tot == this.totcheck) {
      this.byService.subjectMarksCriteria(formdata, this.subjectId).subscribe(
        (result) => {
          this.getSubjects();
          this.showSuccess("Succesfully Updated Marks Criteria for " + this.subjectFullName);
        },
        (err) => {
          console.log("error")
        }
      )
    }
    else {
      this.showError("Total credits should be equal to " + this.totcheck);
    }
  }


  showSuccess(m) {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: m, detail: '' });
  }

  showError(m) {
    this.msgs = [];
    this.msgs.push({ severity: 'error', summary: m, detail: '' });
  }
}
