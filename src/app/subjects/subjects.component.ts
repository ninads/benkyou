import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  subjectsList = [];
  facultyList = [];
  error: string;
  AddSubjectForm: FormGroup;
  display: boolean = false;
  status: string = "Add"
  successmessage: string;
  subId: string;
  msgs = [];
  loading: boolean;

  constructor(private fb: FormBuilder, private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.getFaculty();
    this.getSubjects();
    this.buildForm();

  }

  buildForm() {
    this.AddSubjectForm = this.fb.group({
      'subjectId': ["", Validators.required],
      'fullname': ["", Validators.required],
      'shortname': ["", Validators.required],
      'semester': ["", Validators.required],
      'totalmarks': ["", Validators.required],
    })
  }

  buildUpdateForm(a, b, c, d, e) {
    this.AddSubjectForm = this.fb.group({
      'subjectId': [a, Validators.required],
      'fullname': [b, Validators.required],
      'shortname': [c, Validators.required],
      'semester': [d, Validators.required],
      'totalmarks': [e, Validators.required]
    })
  }


  showDialog() {
    this.display = true;
  }
  hideDialog() {
    this.display = false;
    this.status = "Add"
    this.AddSubjectForm.reset();
  }

  getSubjects() {
    //  this.loading = true;
    this.byService.getSubjects().subscribe(
      (result) => {
        this.subjectsList = result;
        for (let x of this.subjectsList) {
          x.facultyName = [];
        }
        for (const { i, index } of this.subjectsList.map((i, index) => ({ i, index }))) {
          for (let j of this.facultyList) {
            for (let k of j.subjects) {

              if (i.subjectId == k) {
                this.subjectsList[index].facultyName.push(j.facultyName);
              }
            }

          }
        }
        //  this.loading = false;
      },
      (err) => {
        console.log("error");

      }
    )

  }



  getFaculty() {
    this.byService.getFaculty().subscribe(
      (result) => {
        this.facultyList = result;
      },
      (err) => {
        console.log("error");
      }
    )
  }

  updateSubject(id) {
    this.status = "Update";
    this.subId = id;
    this.display = true;
    for (let i of this.subjectsList) {
      if (id == i._id.$oid) {
        let a = i.subjectId;
        let b = i.subjectName.fullName;
        let c = i.subjectName.shortName;
        let d = i.semester;
        let e = i.totalInternalCredits;
        this.buildUpdateForm(a, b, c, d, e);
      }
    }
  }


  submit() {
    this.subjectsList = null;
    this.display = false;
    let formdata = this.AddSubjectForm.value;
    if (this.status == "Add") {
      this.buildForm();
      this.AddNewSubject(formdata.subjectId, formdata.fullname, formdata.shortname, formdata.semester, formdata.totalmarks)
    }

    if (this.status == "Update") {
      this.UpdateSubject(formdata.subjectId, formdata.fullname, formdata.shortname, formdata.semester, formdata.totalmarks, this.subId)
    }
    this.AddSubjectForm.reset();
    this.status = "Add"

  }

  AddNewSubject(id, fname, sname, sem, marks) {
    this.byService.PostSubject(id, fname, sname, sem, marks).subscribe(
      (result) => {
        this.successmessage = fname + " has been added";
        this.showSuccess(this.successmessage);
        this.getSubjects();
      },
      (err) => {
        this.error = "Error in Post Subject";
      }
    )
  }

  UpdateSubject(id, fname, sname, sem, marks, uid) {
    this.byService.UpdateSubject(id, fname, sname, sem, uid, marks).subscribe(
      (result) => {
        this.successmessage = fname + " has been updated";
        this.showSuccess(this.successmessage);
        this.getSubjects();
      },
      (err) => {
        this.error = "Error in Update Subject";
      }
    )
    this.status = "Add"
  }

  showSuccess(m) {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: m, detail: '' });
  }

  deleteSubject(id, name) {
    this.subjectsList = null;
    this.byService.Deletesubject(id).subscribe(
      (result) => {
        this.successmessage = name + " has been deleted"
        this.showSuccess(this.successmessage);
        this.getSubjects();
      },
      (err) => {
        console.log(err);
      }
    )
  }

}
