import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ByServiceService } from '../by-service.service';


@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {

  AddStudentForm: FormGroup;
  showForm: boolean = true;
  success: boolean;
  courseList: string;
  error: string;
  successmessage: string;
  disabledBtn: boolean;
  constructor(private fb: FormBuilder, private byService: ByServiceService) { }

  ngOnInit() {
    this.buildForm();
    this.getCourses();

  }

  buildForm() {
    this.AddStudentForm = this.fb.group({

      'studentName': ["", Validators.required],
      'username': [""],
      'password': [""],
      'rollno': ["", Validators.required],
      'prn': [""],
      'email': ["", Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")],
      'contact': [""],
      'course': ["", Validators.required]
    })
  }

  getCourses(){
    this.byService.getCourses().subscribe(
      (result) => {
      //  console.log(result)
        if (result) {
          this.courseList = result;
//console.log(this.courseList)
        }
        else {
          this.error = "Something went wrong"
       
        }
      },
      (err) => {
        console.log("error");

      }
    )
  }

  addNewStudent(){
    let formData = this.AddStudentForm.value;
    let isDefaulter = false;
    let status = 1;

    // console.log(formData.studentName);
    // console.log(formData.username);
    // console.log(formData.password);
    // console.log(formData.rollno);
    // console.log(formData.prn);
    // console.log(formData.email);
    // console.log(formData.contact);
    // console.log(formData.course);

    this.byService.PostStudent(formData.studentName, formData.username, formData.password,
      formData.rollno, formData.prn, formData.email, formData.contact, isDefaulter, status, formData.course).subscribe(
      (result) => {
          this.successmessage = "Student has been added"
          this.success = true;
          this.disabledBtn = true;
          this.showForm = false;
          setTimeout(() => {
            this.buildForm();
            this.showForm = true;
            this.success = false;
          });
      },
      (err) => {
        this.error = "Error in Post Student";
      }
    )

  }

}
