import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {

  facultyList = [];
  subjectsList = [];
  error: string;
  display: boolean = false;
  subjects: any;
  facultyName: string;
  role: string;
  successmessage: string;
  status: string = "Add";
  upId: string;
  msgs = [];
  constructor(private fb: FormBuilder, private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.getSubjects();
    this.getFaculty();


  }

  showDialog() {
    this.display = true;
  }
  hideDialog() {
    // console.log(this.subjects)
    this.status = "Add"
    this.display = false;
    this.facultyName = null;
    this.subjects = [];
    this.role = "";

  }


  getSubjects() {
    this.byService.getSubjects().subscribe(
      (result) => {
        let temp = result;
        //console.log(temp[0].subjectId)
        for (let i = 0; i < temp.length; i++) {
          this.subjectsList.push({ label: temp[i].subjectName.shortName, value: temp[i].subjectId })
        }
        //        console.log(this.subjectsList)
      },
      (err) => {
        console.log("error");
      }
    )
  }

  getFaculty() {
    let temp = [];
    this.byService.getFaculty().subscribe(
      (result) => {

        for(let i of result){
          if(i.role == "HOD"){
            continue;
          }
          temp.push(i);
        }
        this.facultyList = temp;
      },
      (err) => {
        console.log("error");
      }
    )

  }

  submit(v) {
    this.facultyList = null;
    if (this.status == "Add") {
      this.postFaculty(v.facultyName, v.subjects, v.role);
    }

    else if (this.status == "Update") {
      this.postUpdateFaculty(v.facultyName, v.subjects, v.role);
    }

  }

  postFaculty(fname, sub, role) {
    //console.log(this.subjects, this.facultyName, this.role)
    this.byService.postFaculty(fname, sub, role).subscribe(
      (result) => {
        this.successmessage = "Faculty Added Successfully";
        this.showSuccess(this.successmessage); 
        this.getFaculty();
        setTimeout(() => {
          this.successmessage = null;
        }, 3000);
      },
      (err) => {
        this.error = "Error in Post";
      }
    )

    this.display = false;
  }


  deleteFaculty(id) {
    this.facultyList = null;
    this.byService.Deletefaculty(id).subscribe(
      (result) => {
        this.getFaculty();
        this.successmessage = "Faculty has been deleted"
        this.showSuccess(this.successmessage); 
        setTimeout(() => {
          this.successmessage = null;
        }, 3000);

      },
      (err) => {
        console.log(err);
      }
    )
  }

  postUpdateFaculty(fname, sub, role) {
    this.byService.updateFaculty(fname, sub, role, this.upId).subscribe(
      (result) => {
        this.successmessage = "Faculty Updated Successfully";
        this.showSuccess(this.successmessage); 
        this.showSuccess(this.successmessage); 
        this.getFaculty();
        setTimeout(() => {
          this.successmessage = null;
        }, 3000);
      },
      (err) => {
        this.error = "Error in update";
      }
    )
    this.status = "Add"
    this.display = false;
  }

  updateFaculty(id) {
    for (let i of this.facultyList) {
      if (id == i._id.$oid) {
        this.facultyName = i.facultyName;
        this.subjects = i.subjects;
        this.role = i.role;
        this.status = "Update"
        this.upId = id;
      }
    }
    this.showDialog();
  }

  showSuccess(m) {
    this.msgs = [];
    this.msgs.push({severity:'success', summary:m, detail:''});
}

}

