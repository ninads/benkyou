import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MainloginComponent } from './mainlogin/mainlogin.component';
import { LoginComponent } from './login/login.component';
import { HodComponent } from './hod/hod.component';
import { StudentComponent } from './student/student.component';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule, MatSelectModule, MatProgressSpinnerModule, MatRadioModule, MatProgressBarModule,
  MatButtonModule, MatTableModule
} from '@angular/material';
import { ByServiceService } from './by-service.service';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from './header/header.component';
import { AccordionModule } from "ngx-accordion";
import { ViewstudentsComponent } from './viewstudents/viewstudents.component';
import {
  PaginatorModule, DataTableModule, SharedModule, DialogModule, ButtonModule, ConfirmDialogModule,
  ConfirmationService, GrowlModule, MultiSelectModule, MessagesModule, PickListModule, SpinnerModule
} from 'primeng/primeng';
import { FacultyComponent } from './faculty/faculty.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { AuthGuard } from './auth.guard';
import { AddstudentComponent } from './addstudent/addstudent.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { UpdatestudentComponent } from './updatestudent/updatestudent.component';
import { ProfessorComponent } from './professor/professor.component';
import { SubjectComponent } from './subject/subject.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { RecordattendanceComponent } from './recordattendance/recordattendance.component';
import { StudentattendanceComponent } from './studentattendance/studentattendance.component';
import { AllattendanceComponent } from './allattendance/allattendance.component';
import { MarkstemplateComponent } from './markstemplate/markstemplate.component';
import { SetmarksComponent } from './setmarks/setmarks.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'mainlogin' },
  { path: 'mainlogin', component: MainloginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'hod', component: HodComponent, canActivate: [AuthGuard], children: [
      {
        path: '',
        component: ViewstudentsComponent
      },
      {
        path: 'subjects',
        component: SubjectsComponent
      },
      {
        path: 'addstudent',
        component: AddstudentComponent
      },
      {
        path: 'updatestudent',
        component: UpdatestudentComponent
      },
      {
        path: 'faculty',
        component: FacultyComponent
      },
      {
        path: 'studentAttendance',
        component: StudentattendanceComponent
      },
      {
        path: 'allAttendance',
        component: AllattendanceComponent
      }
    ]
  },
  {
    path: 'professor', component: ProfessorComponent, canActivate: [AuthGuard], children: [
      {
        path: '',
        component: ViewstudentsComponent
      },
      {
        path: 'subject',
        component: SubjectComponent
      },
      {
        path: 'attendance',
        component: AttendanceComponent
      },
      {
        path: 'recordattendance',
        component: RecordattendanceComponent
      },
      {
        path: 'markstemplate',
        component: MarkstemplateComponent
      },
      {
        path: 'setmarks',
        component: SetmarksComponent
      },

    ]
  },
  { path: '**', component: PathNotFoundComponent },


]

@NgModule({
  declarations: [
    AppComponent,
    MainloginComponent,
    LoginComponent,
    HodComponent,
    StudentComponent,
    PathNotFoundComponent,
    HeaderComponent,
    ViewstudentsComponent,
    FacultyComponent,
    SubjectsComponent,
    AddstudentComponent,
    UpdatestudentComponent,
    ProfessorComponent,
    SubjectComponent,
    AttendanceComponent,
    RecordattendanceComponent,
    StudentattendanceComponent,
    AllattendanceComponent,
    MarkstemplateComponent,
    SetmarksComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    HttpModule,
    AccordionModule,
    PaginatorModule,
    DataTableModule,
    SharedModule,
    ButtonModule,
    MatSelectModule,
    ConfirmDialogModule,
    GrowlModule,
    DialogModule,
    MultiSelectModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MessagesModule,
    PickListModule,
    SpinnerModule,
    MatButtonModule,
    MatTableModule
  ],
  providers: [ByServiceService, AuthGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
