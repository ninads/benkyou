import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-mainlogin',
  templateUrl: './mainlogin.component.html',
  styles: [`
    .root {
      background: blue;
      width: 100%;
    }
  `]
})
export class MainloginComponent implements OnInit {

  constructor(private router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.setBackgroundImage();
  }

  // Remove the class from body tag when the View is destroyed
  ngOnDestroy() {
    this.removeBackgroundImage();
  }


  // Set background image to mainlogin page
  setBackgroundImage() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.add('login-img');
  }

  // Destroy background image to mainlogin page
  removeBackgroundImage() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove("login-img");
  }

  hod() {
    localStorage.setItem('role', 'hod')
    this.router.navigateByUrl('/login');
}

professor() {
  localStorage.setItem('role', 'professor')
  this.router.navigateByUrl('/login');
}


}
