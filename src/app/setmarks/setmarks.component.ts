import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ByServiceService } from '../by-service.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-setmarks',
  templateUrl: './setmarks.component.html',
  styleUrls: ['./setmarks.component.css']
})
export class SetmarksComponent implements OnInit {

  subjectName: string;
  searchForm: FormGroup;
  display: boolean;
  maxError: boolean;
  flag = 0;
  studentList = [];
  subjectId: string;
  subjectFullName: string;
  studentIdUpdate: string;
  totcheck: number;
  msgs = [];
  studentNameUpdate: string;
  markstemplate = [];

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private _router: Router,
    private byService: ByServiceService) { }


  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.subjectName = params['id'];
      this.getStudents();
      this.getSubjects();
      this.searchForm = this.fb.group({
        marks: this.fb.array([])
      });
    });
  }



  getStudents() {
    this.byService.getStudents()
      .subscribe(
      (result) => {
        //  console.log(result)
        if (result) {
          this.studentList = result;
        }
      },
      err => {
        console.log("error");
      }
      )
  }

  getSubjects() {
    //  this.loading = true;
    this.byService.getSubjectByShortName(this.subjectName).subscribe(
      (result) => {
        let subjectsList = result;
        this.subjectId = subjectsList[0]._id.$oid;
        this.subjectFullName = subjectsList[0].subjectName.fullName;
        this.totcheck = subjectsList[0].totalInternalCredits;
        this.markstemplate = subjectsList[0].marksTemplate;
        //   console.log(this.markstemplate)
      },
      (err) => {
        console.log("error");
      }
    )
  }

  initPropGroup() {
    return this.fb.group({
      marks: ["NA"]
    })
  }

  updateMarks(id, name) {
    this.searchForm = this.fb.group({
      marks: this.fb.array([])
    });
    this.studentIdUpdate = id;
    this.studentNameUpdate = name;

    for (let i of this.markstemplate) {
      const control = <FormArray>this.searchForm.controls['marks'];
      control.push(this.initPropGroup())
    }
    this.display = true;
  }

  postMarks() {
    this.maxError = false;
    let totalMarks = 0;
    let totalMaxMarks = 0;
    let formData = this.searchForm.value.marks;
    let previousMarksList = [];

    for (let x of this.studentList) {
      if (x.name == this.studentNameUpdate) {
        if (!x.credits || x.credits.length == 0) {
          this.flag = 1;
        }
        else {
for(let j in x.credits){

  if(x.credits[j].subject == this.subjectName)
  {
  //  console.log(j, x.credits)
  x.credits.splice(j, 1);
 // console.log(x.credits)
  previousMarksList = x.credits;
  }
  else{
    previousMarksList = x.credits;
  }
}

        }

      }
    }

    for (let i in formData) {
      if (formData[i].marks == null) {
        formData[i].marks = "NA"
      }
      formData[i].name = this.markstemplate[i].name;
      formData[i].maxMarks = Number(this.markstemplate[i].total);
      if (formData[i].marks != "NA") {
        if (formData[i].marks > formData[i].maxMarks) {
          this.maxError = true;
          this.showError("Please Make sure marks do not exceed max marks displayed on the right");
          break;
        }
        totalMarks += formData[i].marks;
        totalMaxMarks += formData[i].maxMarks

      }
    }
    let temp = {
      "subject": this.subjectName,
      "totalMarks": totalMarks,
      "totalMaxMarks": totalMaxMarks,
      "criteria": formData
    }
    

    let finalMarks = [];
    finalMarks = previousMarksList;
    finalMarks.push(temp);
    if (this.maxError == false) {
      this.byService.updateMarks(finalMarks, this.studentIdUpdate).subscribe(
        (result) => {
          this.getSubjects();
          this.showSuccess("Succesfully Updated Marks for " + this.studentNameUpdate);
          this.studentList = [];
          this.getStudents();
          this.display = false;
        },
        (err) => {
          console.log("error")
        }
      )
    }
   // console.log(finalMarks)

  }

  showSuccess(m) {
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: m, detail: '' });
  }

  showError(m) {
    this.msgs = [];
    this.msgs.push({ severity: 'error', summary: m, detail: '' });
  }

}
