import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetmarksComponent } from './setmarks.component';

describe('SetmarksComponent', () => {
  let component: SetmarksComponent;
  let fixture: ComponentFixture<SetmarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetmarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetmarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
