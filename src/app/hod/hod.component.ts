import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-hod',
  templateUrl: './hod.component.html',
  styleUrls: ['./hod.component.css']
})
export class HodComponent implements OnInit {
  name: string;
  constructor() { }

  ngOnInit() {
}

ngOnDestroy(){
  localStorage.clear();
}

}
