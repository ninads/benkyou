import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-viewstudents',
  templateUrl: './viewstudents.component.html',
  styleUrls: ['./viewstudents.component.css']
})
export class ViewstudentsComponent implements OnInit {

  error: string;
  studentList = [];
  successmessage: string;
  success: boolean;
  role: string;
  constructor(private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.role = localStorage.getItem("role")
    this.getStudents();
    // console.log(this.role)
  }

  getStudents() {
    this.byService.getStudents()
      .subscribe(
      (result) => {
        //  console.log(result)
        if (result) {
          this.studentList = result;
        }
      },
      err => {
        console.log("error");
      }
      )
  }

  editStudent(car) {
    console.log(car);
  }

  DeleteStudent(val: string) {
    this.studentList = null;

    this.byService.DeleteStudent(val).subscribe(
      (result) => {
        this.getStudents();
        this.successmessage = "Student has been deleted"
        this.success = true;
      },
      (err) => {
        console.log(err);
      }
    )
  }

}
