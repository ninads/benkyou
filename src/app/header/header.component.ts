import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name: string;
  constructor(private router: Router) { }

  ngOnInit() {
    this.getname();
  }
  getname() {
    this.name = localStorage.getItem("name");
  }

  logout(){
        localStorage.clear();
        this.router.navigateByUrl('/mainlogin');
  }
}
