import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ByServiceService } from '../by-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Message} from 'primeng/components/common/api';


@Component({
  selector: 'app-updatestudent',
  templateUrl: './updatestudent.component.html',
  styleUrls: ['./updatestudent.component.css']
})
export class UpdatestudentComponent implements OnInit {
  AddStudentForm: FormGroup;
  showForm: boolean = true;
  success: boolean;
  courseList: string;
  error: string;
  successmessage: string;
  disabledBtn: boolean;
  studentId: string;
  studentName: string;
  username: string;
  password: string;
  rollno: string;
  prn: string;
  email: string;
  contact: string;
  course: string;
  msgs: Message[] = [];

  constructor(private fb: FormBuilder, private byService: ByServiceService, private activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.buildForm();
    this.getStudentId();
    this.getStudentById();
    this.getCourses();

  }

  getStudentId() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.studentId = params['id'];
    });
  }

  getStudentById() {
    this.byService.getStudentById(this.studentId).subscribe(
      (result) => {
        //console.log(result);
        this.studentName = result.name;
        this.username = result.username;
        this.password = result.password;
        this.rollno = result.roll_no;
        this.prn = result.PRN;
        this.email = result.email;
        this.contact = result.contact;
        this.course = result.courseId;

        this.showForm = false;
        setTimeout(() => {
          this.buildForm();
          this.showForm = true;
        });

      },
      (err) => {
        console.log("error");

      }
    )

  }

  buildForm() {
    this.AddStudentForm = this.fb.group({

      'studentName': [this.studentName, Validators.required],
      'username': [this.username],
      'password': [this.password],
      'rollno': [this.rollno, Validators.required],
      'prn': [this.prn],
      'email': [this.email, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")],
      'contact': [this.contact],
      'course': [this.course, Validators.required]
    })
  }

  getCourses() {
    this.byService.getCourses().subscribe(
      (result) => {
        //  console.log(result)
        if (result) {
          this.courseList = result;

        }
        else {
          this.error = "Something went wrong"

        }
      },
      (err) => {
        console.log("error");

      }
    )
  }

  updateStudent() {
    let formData = this.AddStudentForm.value;

    // console.log(formData.studentName);
    // console.log(formData.username);
    // console.log(formData.password);
    // console.log(formData.rollno);
    // console.log(formData.prn);
    // console.log(formData.email);
    // console.log(formData.contact);
    // console.log(formData.course);

    this.byService.UpdateStudent(formData.studentName, formData.username, formData.password,
      formData.rollno, formData.prn, formData.email, formData.contact, formData.course, this.studentId).subscribe(
      (result) => {
          this.getStudentById();
          this._router.navigateByUrl('/hod');   
      },
      (err) => {
        this.error = "Error in update";
      }
      )

  }


}
