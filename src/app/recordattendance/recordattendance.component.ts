import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-recordattendance',
  templateUrl: './recordattendance.component.html',
  styleUrls: ['./recordattendance.component.css']
})
export class RecordattendanceComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private _router: Router, private byService: ByServiceService) { }
  subjectId: string;
  subjectName: string;
  absentList = [];
  presentList = [];
  finalList = [];
  hours: number;
  attendance = [];
  studentList = [];
  hoursattended: number = 0;
  hourscounted: number = 0;
  arr = [];
  ngOnInit() {
    this.getStudents();
    this.getSubjectname();
    this.setSubjectId();
    this.getAttendance();
  }
  getSubjectname() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.subjectName = params['id'];
    });
  }

  setSubjectId() {
    this.byService.getSubjectName()
      .subscribe(
      (result) => {
        if (result) {
          //   console.log(result)
          for (let i of result) {
            if (i.subjectName.shortName == this.subjectName) {
              this.subjectId = i.subjectId;
            }
          }
        }
      },
      err => {
        console.log("error");
      }
      )
  }

  getStudents() {
    this.byService.getStudents()
      .subscribe(
      (result) => {
        //  console.log(result)
        if (result) {
          this.studentList = result;
        }
      },
      err => {
        console.log("error");
      }
      )
  }

  getAttendance() {
    this.byService.getAttendance()
      .subscribe(
      (result) => {
        if (result) {
          if (result.length < 1) {
let temp = []
            for (let i of this.studentList) {
              temp.push({ "studentName": i.name, "roll_no": i.roll_no })
            }
            this.absentList = temp;
          }
          else{
            this.attendance = result;
            this.absentList = this.attendance;
          }
        }
      },
      err => {
        console.log("error");
      }
      )
  }



  submit() {
    this.finalList = [];
    this.pushPresent(this.presentList);
  }

  pushPresent(x) {
let flag = 0;
console.log(x)
    for (let i of x) {
      this.arr = [];
      //console.log(this.arr)
      if(i.subjects){
      for (let j in i.subjects) {
        if (i.subjects[j].id == this.subjectId) {
          this.arr = [];
          flag = 1;
          this.hourscounted = i.subjects[j].hours_counted + this.hours;
          this.hoursattended = i.subjects[j].hours_attended + this.hours;
          i.subjects.splice(j, 1);
          this.arr = i.subjects
       //   console.log(this.arr)
        }
      }
    }
      if(flag == 0 ){
        if(i.subjects){
        this.arr = i.subjects
        this.hourscounted = this.hours;
        this.hoursattended = this.hours;
        }
        if(!i.subjects || this.attendance.length < 1){
          this.hourscounted = this.hours;
          this.hoursattended = this.hours;
        }
      }

      this.arr.push(
        {
          id: this.subjectId,
          name: this.subjectName,
          hours_attended: this.hoursattended,
          hours_counted: this.hourscounted
        })
      this.finalList.push(
        {
          roll_no: i.roll_no,
          studentName: i.studentName,
          subjects: this.arr
        }
      )
    }
  // console.log(this.finalList)
this.pushAbsent(this.absentList);

  }

  pushAbsent(x) {
    let flag = 0;
    for (let i of x) {
      this.arr = [];
      //console.log(this.arr)
      if(i.subjects){
      for (let j in i.subjects) {
        if (i.subjects[j].id == this.subjectId) {
          this.arr = [];
          flag = 1;
          this.hourscounted = i.subjects[j].hours_counted + this.hours;
          // console.log(i.studentName ,i.subjects[j].hours_attended)
          this.hoursattended = i.subjects[j].hours_attended;
          i.subjects.splice(j, 1);
          this.arr = i.subjects
       //   console.log(this.arr)
        }
      }
    }
      if(flag == 0 ){
        if(i.subjects){
        this.arr = i.subjects
        this.hourscounted = this.hours;
        this.hoursattended = 0;
        }
        if(!i.subjects || this.attendance.length < 1){
          this.hourscounted = this.hours;
          this.hoursattended = 0;
        }
      }

      this.arr.push(
        {
          id: this.subjectId,
          name: this.subjectName,
          hours_attended: this.hoursattended,
          hours_counted: this.hourscounted
        })
      this.finalList.push(
        {
          roll_no: i.roll_no,
          studentName: i.studentName,
          subjects: this.arr
        }
      )
    }
   console.log(this.finalList)
   this.postAttendance(this.finalList)
  }

  postAttendance(a) {
    let t = JSON.stringify(a)
    console.log(t)

    this.byService.PostAttendance(t).subscribe(
      (result) => {
        console.log(result);
      },
      (err) => {
        console.log("error in posting attendance")
      }
    )
    this._router.navigate(['/professor/attendance/'],{ queryParams: { id: this.subjectName } });
  }
}

// }
