import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordattendanceComponent } from './recordattendance.component';

describe('RecordattendanceComponent', () => {
  let component: RecordattendanceComponent;
  let fixture: ComponentFixture<RecordattendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordattendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordattendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
