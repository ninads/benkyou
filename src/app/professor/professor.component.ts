import { Component, OnInit, OnDestroy } from '@angular/core';
import { ByServiceService } from '../by-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.css']
})
export class ProfessorComponent implements OnInit {
  subjectIds = [];
  subjects=[];
  sub = [];

  constructor(private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.subjects = JSON.parse(localStorage.getItem("subjectList"))
    this.subjectIds = JSON.parse(localStorage.getItem("subjects"))
    // this.getSubjectName()
    this.createSubjectList()

  }
  ngOnDestroy() {
    localStorage.clear();
  }


  createSubjectList() {
    let t = [];

    if(this.subjectIds && this.subjects){
    for (let i of this.subjectIds) {
      for (let j of this.subjects) {
        if(i == j.subjectId){
          t.push({"id": i, "name": j.subjectName.shortName})
        }
      }
    }
    this.sub = t;
  }
}

}
