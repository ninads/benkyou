import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
subjectId: string;
attendanceList = [];
  constructor(private activatedRoute: ActivatedRoute, private _router: Router, private byService: ByServiceService) { }

  ngOnInit() {
    this.getAttendance();
    this.getSubjectId();

  }

  getSubjectId() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.subjectId = params['id'];
      this.getAttendance();
    });
    
  }

  getAttendance() {
    this.attendanceList = [];
    this.byService.getAttendance()
      .subscribe(
      (result) => {
        if (result) {
          let a = [];
          let counted = 0;
          let attended = 0;
          let percent = "";
          let ps = 0;
          let statuss = 1;

          for(let i of result){
            statuss = 1;
            for(let j of i.subjects){
              
              if(j.name == this.subjectId){
              counted = j.hours_counted;
              attended = j.hours_attended;
              ps = attended/counted*100;
              if(ps < 75){
                statuss = 0;
              }
              percent = ps.toFixed(2) + "%";
              }
            }

a.push({name: i.studentName, counted: counted, attended: attended, percent: percent, status: statuss })
          }
      //    console.log(a)
this.attendanceList = a;
        }
      },
      err => {
        console.log("error");
      }
      )
  }


}
