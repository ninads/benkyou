import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ByServiceService } from '../by-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: boolean;
  loginForm: FormGroup;
  msgs = [];
  constructor(private _router: Router, private fb: FormBuilder, private byService: ByServiceService, protected service: ByServiceService) { }

  ngOnInit() {
    this.setBackgroundColor();
    this.buildForm();
  }

  //Remove the class from body tag when the View is destroyed
  ngOnDestroy() {
    this.removeBackgroundColor();
  }

  setBackgroundColor() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.add("bg-536ACC");
  }

  //Destroy background image to mainlogin page
  removeBackgroundColor() {
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove("bg-536ACC");
  }

  buildForm() {
    this.loginForm = this.fb.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required]
    })
  }

  postLogin() {
    let formData = this.loginForm.value;
    if (localStorage.getItem('role') == 'hod') {
      this.loginHOD(formData.username, formData.password);
    }
    if (localStorage.getItem('role') == 'professor') {
      this.loginProfessor(formData.username, formData.password);
    }

  }

  loginHOD(username, password) {
    this.byService.authenticateFaculty(username, password).subscribe(
      (result) => {
        // console.log(result)

        if (result[0].role == "HOD") {
          localStorage.setItem("name", result[0].facultyName);
          localStorage.setItem("loggedIn", "true");
          this._router.navigateByUrl('/hod');
        }
        else {
          this.showError();
        }
      },
      (err) => {


      }
    )
  }


  loginProfessor(username, password) {


    this.byService.getSubjectName().subscribe(
      (result) => {
localStorage.setItem("subjectList", JSON.stringify(result))
      },
      (err) => {
        console.log("error");
      }
    )

    
    this.byService.authenticateFaculty(username, password).subscribe(
      (result) => {
        let a = [];
if(result[0]){
        if (result[0].username == username) {
          a = result[0].subjects; 
          localStorage.setItem("name", result[0].facultyName);
          localStorage.setItem("loggedIn", "true");
          localStorage.setItem("subjects", JSON.stringify(a))
          this._router.navigateByUrl('/professor');
        }
      }
        else {
          this.showError();
        }
      },
      (err) => {


      }
    )


  
  }

  showError() {
    this.msgs = [];
    this.msgs.push({ severity: 'error', summary: 'Login Failed', detail: 'Username/Password Incorrect' });
  }

}
