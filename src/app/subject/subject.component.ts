import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  subjectId: string;

  constructor(private activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.getSubjectId();
  }

  getSubjectId() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.subjectId = params['id'];
    });
  }

}
