import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ByServiceService {
  Url: String = "https://api.mlab.com/api/1/";

  constructor(private http: Http, private options: RequestOptions) { }

  authenticateFaculty(u, p) {
    let qry = JSON.stringify({ "username": u, "password": p });
    return this.http.get(this.Url + 'databases/benkyou/collections/faculty?q=' + qry + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in login');
        }
      });
  }

  getStudents() {
    let q = JSON.stringify({ "username": 0, "password": 0 });
    let s = JSON.stringify({ "roll_no": 1 })
    return this.http.get(this.Url + 'databases/benkyou/collections/students?f=' + q + '&s=' + s + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching students');
        }
      });
  }

  getStudentById(id) {
    return this.http.get(this.Url + 'databases/benkyou/collections/students/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching students');
        }
      });
  }

  updateMarks(temp, uid) {
    var data = {
      $set: { credits: temp }
    };
    return this.http.put(this.Url + 'databases/benkyou/collections/students/' + uid + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in put');
        }
      });
  }


  DeleteStudent(studentid: String) {

    var id = studentid;
    return this.http.delete(this.Url + 'databases/benkyou/collections/students/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in deleting amenity');
        }
      });
  }

  getSubjects() {
    let s = JSON.stringify({ "subjectId": 1 })
    return this.http.get(this.Url + 'databases/benkyou/collections/subjects?s=' + s + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching subjects');
        }
      });
  }

  getSubjectByShortName(name) {
    let q = JSON.stringify({ "subjectName.shortName": name });
    return this.http.get(this.Url + 'databases/benkyou/collections/subjects?q=' + q + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching short name');
        }
      });
  }

  getSubjectName() {
    let f = JSON.stringify({ "subjectId": 1, "subjectName": 1 })
    return this.http.get(this.Url + 'databases/benkyou/collections/subjects?f=' + f + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching subjects');
        }
      });
  }

  PostSubject(id, fname, sname, sem, marks) {
    var data = {
      subjectId: id, subjectName: { fullName: fname, shortName: sname }, semester: sem, totalInternalCredits: marks
    };
    return this.http.post(this.Url + 'databases/benkyou/collections/subjects?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  UpdateSubject(id, fname, sname, sem, uid, marks) {
    var data = {
      $set: { subjectId: id, subjectName: { fullName: fname, shortName: sname }, semester: sem, totalInternalCredits: marks }
    };
    return this.http.put(this.Url + 'databases/benkyou/collections/subjects/' + uid + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  subjectMarksCriteria(temp, uid) {
    var data = {
      $set: { marksTemplate: temp }
    };
    return this.http.put(this.Url + 'databases/benkyou/collections/subjects/' + uid + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }



  Deletesubject(id: String) {
    return this.http.delete(this.Url + 'databases/benkyou/collections/subjects/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in deleting subject');
        }
      });
  }

  getFaculty() {

    return this.http.get(this.Url + 'databases/benkyou/collections/faculty?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching faculty');
        }
      });
  }

  postFaculty(Name: string, Subjects: any, Type: string) {
    var data = {
      facultyName: Name, subjects: Subjects, role: Type
    };
    return this.http.post(this.Url + 'databases/benkyou/collections/faculty?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  updateFaculty(Name: string, Subjects: any, Type: string, id: string) {
    var data = {
      $set: { facultyName: Name, subjects: Subjects, role: Type }
    };
    return this.http.put(this.Url + 'databases/benkyou/collections/faculty/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in put');
        }
      });
  }

  Deletefaculty(facultyid: String) {
    var id = facultyid;
    return this.http.delete(this.Url + 'databases/benkyou/collections/faculty/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in deleting faculty');
        }
      });
  }

  getCourses() {
    return this.http.get(this.Url + 'databases/benkyou/collections/courses?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching subjects');
        }
      });
  }



  PostStudent(studentName: string, username: string, password: string, rollno: string,
    prn: string, email: string, contact: string, isdefaulter: boolean, status: number, course: string) {

    var data = {
      name: studentName, username: username, password: password, roll_no: rollno,
      PRN: prn, email: email, contact: contact, is_defaulter: isdefaulter, status: status, courseId: course
    };
    return this.http.post(this.Url + 'databases/benkyou/collections/students?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  UpdateStudent(studentName: string, username: string, password: string, rollno: string,
    prn: string, email: string, contact: string, course: string, id: string) {

    var data = {
      $set: {
        name: studentName, username: username, password: password, roll_no: rollno,
        PRN: prn, email: email, contact: contact, courseId: course
      }
    };
    return this.http.put(this.Url + 'databases/benkyou/collections/students/' + id + '?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  PostAttendance(attendance) {

    var data = JSON.parse(attendance);
    return this.http.put(this.Url + 'databases/benkyou/collections/attendance?apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy', data)
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in post');
        }
      });
  }

  getAttendance() {
    let s = JSON.stringify({ "roll_no": 1, "subjects.id": 1 })
    return this.http.get(this.Url + 'databases/benkyou/collections/attendance?s=' + s + '&apiKey=HrbxBQRr7l36xkJFd4NY9e2HmVxW1oiy')
      .map(res => res.json())
      .catch(e => {
        if (e.status === 401) {
          return Observable.throw('error in fetching subjects');
        }
      });
  }



}
