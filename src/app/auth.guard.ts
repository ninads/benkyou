import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router){}
  canActivate() {
   if (localStorage.getItem('loggedIn') == "true") {
     // logged in so return true
     return true;
   }

   else{
   // not logged in so redirect to login page
   this.router.navigate(['/mainlogin']);
   return false;
   }
 }
}
//loggedIn